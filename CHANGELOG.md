# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# 1.1.0 (2019-04-15)


### Features

* Added bitbucket pipeline config/dependency, eslint config ([67e31d9](https://bitbucket.org/autautdoo/dynamic-middleware/commits/67e31d9)), closes [#IGPIMP-197](https://bitbucket.org/autautdoo/dynamic-middleware/issue/IGPIMP-197)
