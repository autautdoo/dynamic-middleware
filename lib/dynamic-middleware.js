/**
 * Created by domagoj on 1/10/17.
 */

/**
 * This method allows creating named middlewares for nodejs express framework.
 * Since express middlewares are immutable,
 * this piece of code can help overcoming that limitation,
 * and dinamically enable/disable middlewares or
 * even change middleware's behaviours.
 *
 * Below is an example of use:
 *
 * let app = express();
 * app.use(DynamicMiddleware.create('hello', function (req, res, next) {
 *      console.log('hello world!');
 *      next();
 * }));
 *
 * // Now, if we want to disable defined middleware, we could use:
 * DynamicMiddleware.get('hello').disable();
 * // or if we wanted to modify middleware method, we'd do it the following way:
 * DynamicMiddleware.get('hello').method = function(req, res, next) {
 *      console.log('hello hello!');
 *      next();
 * }
 */
class DynamicMiddleware {
  /**
     * Creates a new named middleware and stores it locally,
     * so that later it can be accessed by provided name.
     * @param name      Middleware name
     * @param method    Method which will be associated with the middleware
     * @returns {function}
     */
  static create(name, method) {
    const middleware = new Middleware(name, method);
    this._items[name] = middleware;
    return middleware.method;
  }

  /**
     * Gets middleware object with specified name.
     * Useful when you want to manipulate with existing middleware
     * (e.g. disable it or change its method)
     * @param name      The name of existing middleware you want to get
     * @returns {Middleware}
     */
  static get(name) {
    return this._items[name];
  }

  /**
     * Returns array of defined middleware names
     * @returns {Array}
     */
  static get items() {
    return Object.keys(this._items);
  }

  /**
     * Enables DynamicMiddleware to be used in for of construct
     */
  static [Symbol.iterator]() {
    const items = this.items;
    let index = 0;

    return {
      'next': () => ({
        'value': this._items[items[index]],
        'done': ++index > items.length,
      }),
    };
  }
}
DynamicMiddleware._items = {};


class Middleware {
  constructor(name, method) {
    this._name = name;
    this._method = method;
    this._enabled = true;
  }

  get name() {
    return this._name;
  }

  get isEnabled() {
    return this._enabled;
  }

  /**
     * Enables this middleware
     * @returns {Middleware}
     */
  enable() {
    this._enabled = true;
    return this;
  }

  /**
     * Disables this middleware
     * @returns {Middleware}
     */
  disable() {
    this._enabled = false;
    return this;
  }

  /**
     * Associates this middleware with another method.
     * This method enables changing middleware
     * behaviour during runtime.
     * @param method    New function that you want to assign to this middleware
     *                  instead of one that was previously active.
     */
  set method(method) {
    if (typeof method !== 'function') {
      throw new Error('method argument has to be function');
    }
    this._method = method;
  }

  /**
     * Gets middleware method
     */
  get method() {
    return (req, res, next) => {
      if (this._enabled) {
        return this._method(req, res, next);
      } else {
        return next();
      }
    };
  }
}

module.exports = DynamicMiddleware;
